package midianet.journey;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class Application {
	Logger log = Logger.getLogger(getClass().getName());

	public static void main(String[] args) {
		ApiContextInitializer.init();
		new SpringApplicationBuilder(Application.class).run(args);
	}

	@Autowired
	public void init(final JourneyBot bot) {
		try {
			TelegramBotsApi api = new TelegramBotsApi();
			log.info("Starting bot...");
			api.registerBot(bot);
			log.info("Bot started.");
		} catch (TelegramApiException e) {
			log.log(Level.SEVERE,e.getMessage(), e);
		}
	}

}