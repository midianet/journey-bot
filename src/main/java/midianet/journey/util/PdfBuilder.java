package midianet.journey.util;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class PdfBuilder {
	
	public static File create(String name , String cpf, String date) throws Exception {
		OutputStream fos;
		PdfWriter writer;
		PdfDocument pdfdoc;
		Document doc = null;
		String filename=  String.format("/tmp/%s.pdf",cpf);
		fos = new FileOutputStream(new File(filename));
		writer = new PdfWriter(fos);
		pdfdoc = new PdfDocument(writer);
		try {
			StringBuilder b = new StringBuilder();
			doc = new Document(pdfdoc);
			PdfFont fontHeader = PdfFontFactory.createFont("Helvetica-Bold");
			PdfFont fontBody   = PdfFontFactory.createFont("Helvetica");
			doc.add(new Paragraph("Termo de responsabilidade de viagem").setFont(fontHeader).setFontSize(22).setTextAlignment(TextAlignment.CENTER));
			doc.add(new Paragraph("Latinoware 2018").setFont(fontHeader).setFontSize(26).setTextAlignment(TextAlignment.CENTER));
			b.append("1. Esta caravana existe em função do evento Latinoware 2018, os horários das paradas e traslados durante toda a viagem serão ")
			 .append("estratégicos ao evento.\n")
			 .append("Todos os participamentes terão direito à 2 (dois) dias livres antes do ")
			 .append("evento para interesses pessoais, pois será exigido a participação mínima ")
			 .append("de 83% (oitenta e três por cento) equivalentes a dois dias e meio nas palestras, comprovadas pelas ")
			 .append("respectivas inscrições, caso essa obrigação não seja comprovada ou ")
			 .append("justificada, ficará o participante impedido de retornar com malas no ")
			 .append("bagageiro do ônibus e somente será permitido uma bagagem de mão ")
			 .append("pequena com itens de higiene pessoal e alimentos.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("2. A caravana não tem fins comerciais e lucrativos, e em virtude de boa ")
			 .append("fé todo participante deverá portar no retorno declaração da Receita ")
			 .append("Federal com a quota limite de U$300,00 (trezentos dólares), mesmo que ")
			 .append("nada tenha sido adquirido, assim isentando a empresa de ônibus e os ")
			 .append("demais participantes de quaisquer transtornos sobre importação ilegal de ")
			 .append("produtos e bens de consumo, caso o participante não tenha posse desse ")
			 .append("documento ficará impedido de subir suas malas no bagageiro ônibus, ")
			 .append("ficando sobre sua responsabilidade o paradeiro das mesmas somente ")
			 .append("poderá subir bagagens identificadas ao proprietário e lacradas conforme a resolução 1432.26-2006 artigo primeiro e seu ")
			 .append("conteúdo e de total responsábilidade do identificado. ");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("3. De acordo com a norma da ANTT fica estipulado o máximo de 2 ")
             .append("(duas) malas médias ou 60 Kg por passageiro (sujeito a pesagem), caso ")
			.append("esse limite seja ultrapasado o participante deverá se responsabilizar ")
			.append("sobre o paradeiro do excedente.\n\n");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("4. Fica proibido ao participante atos de violência, atos obscenos, ")
			.append("palavras ofensivas e apologias como estupro,racismo e outros e caso ")
			.append("aconteça a organização poderá arbitrar para que o mesmo deixe a ")
			.append("caravana na próxima parada que tenha recurso de retorno.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("5. De acordo com a Lei Federal 9294/96, é proibido o uso de cigarros, ")
			.append("cigarrilhas, charutos, cachimbos ou qualquer outro produto fumígeno, ")
			.append("derivado ou não do tabaco, no ônibus.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("6. De acordo com a Lei Federal n. 891/38 é proibido uso de drogas ")
		 	 .append("e entorpecentes ou substâncias ilícitas durante toda a viagem ")
			 .append("principalmente no ônibus, dependências do hotel e evento.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("7. Será permitido o uso de bebidas alcoólicas no ônibus desde que o ")
			 .append("participante não venha a passar dos limites (em estado de embriaguês) ")
			 .append("caso isso seja percebido o mesmo será advertido pelos organizadores o ")
		 	 .append("participante poderá arbitrar para que o mesmo deixe a caravana na ")
			 .append("próxima parada que tenha recursos de retorno. Fica proibido o uso de ")
			 .append("garrafas e recipientes de vidro no interior do ônibus.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("8. O ônibus não possui janelas, por esse motivo todo participante será ")
			 .append("responsável pela coleta do seu próprio lixo principalmente das bebidas, ")
			 .append("será disponibilizados sacos de lixo que deverão ser descartados ")
			 .append("obrigatoriamente em todas as paradas, inclusive na chegada, eliminando ")
			 .append("o cheiro desagradável.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("9. Uma caixa de isopor oficial será disponibilizada na sala de jogos do ")
			 .append("ônibus, fica proibido o participante levar sua própria caixa de isopor.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("10.Fica acordado entre os participantes que a caravana estará equipada ")
			 .append("com som e iluminação pirotécnica, ligados até um período de comum ")
			 .append("acordo, ficando assim individualmente um ou outro participante sem o ")
			 .append("direito de reclamar por sossego ou silêncio.\n\n\n");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("11.Todo participante deverá se atentar pelos horários de saída de cada ")
			 .append("parada, principalmente no ponto inicial e o início da volta,ficando o ")
			 .append("mesmo sujeito a ser deixado pela caravana e o mesmo ficará ")
			.append("responsável pelo seu retorno.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("12.Esta caravana limita-se ao território nacional, ficando sobre ")
			 .append("responsábilidade total do participante uma eventual saída do país.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("13.Como esta caravana não tem fins lucrativos, fica acordado entre os ")
			.append("participantes e organização responsabilidades sobre danos e prejuizos ")
			.append("ao patrimônio de terceiros como ônibus, dependências do hotel e evento, ")
			.append("assumindo de igual responsabilidade, sendo assim todo participante tem ")
			.append("o dever de fiscalizar e zelar.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("14.Todo participante deverá contribuir com o clima amigavel e ")
			.append("descontraído que a caravana mantém por muitos anos durante toda a ")
			.append("viagem, caso algum participante contrarie o bom senso e os bons ")
			.append("costumes, a coordenação poderá intervir com advertência em caso de ")
			.append("reincidência a coordenação poderá arbitrar para que o mesmo deixe a ")
			 .append("caravana na próxima parada que tenha recurso de retorno.");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("Eu ").append(name).append(" portador do cpf ").append(cpf).append(" declaro estar ")
			.append("ciente de todos os termos e condições acima e confirmo o valor legal ")
			.append("quando assinado digitalmente ao clicar no botão (Concordo) abaixo.\n\n");
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
			b = new StringBuilder();
			b.append("Goiânia ").append(date);
			doc.add(new Paragraph(b.toString()).setFont(fontBody).setFontSize(16).setTextAlignment(TextAlignment.JUSTIFIED));
		}finally {
			doc.close();
		}
		return new File(filename);
	}
	
}