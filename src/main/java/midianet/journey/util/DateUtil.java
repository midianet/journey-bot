package midianet.journey.util;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class DateUtil {
	
	public static void main(String[] args) {
		System.out.println(DateUtil.nowExtense());
	}
	
	public static String nowExtense(){
		Map<Integer,String> months = new HashMap<>();
		months.put(1,"Janeiro");
		months.put(2,"Fevereiro");
		months.put(3,"Março");
		months.put(4,"Abril");
		months.put(5,"Maio");
		months.put(6,"Junho");
		months.put(7,"Julho");
		months.put(8,"Agosto");
		months.put(9,"Setembro");
		months.put(10,"Outubro");
		months.put(11,"Novembro");
		months.put(12,"Dezembro");
		LocalDate date = LocalDate.now();
		return String.format("%d de %s de %d",date.getDayOfMonth(), months.get(date.getMonthValue()), date.getYear());
	}
}
