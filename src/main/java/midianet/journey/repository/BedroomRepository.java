package midianet.journey.repository;

import midianet.journey.domain.Bedroom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class BedroomRepository {
    private Logger log = LoggerFactory.getLogger(getClass());

    private StringBuilder sqlDefault = new StringBuilder()
        .append("select id,")
        .append("       description,")
        .append("       type,")
        .append("       gender")
        .append("  from bedroom");

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    public Optional<Bedroom> findById(Long id){
        log.trace("Request findById Bedroom " + id);
        StringBuilder sql = new StringBuilder(sqlDefault.toString())
           .append(" where id = :id");
        Map<String,Object> param = new HashMap();
        param.put("id",id);
        log.debug(sql.toString() + " " + id);
        try{
            return Optional.of(jdbc.queryForObject(sql.toString(),param, this::mapRow));
        }catch(EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    private Bedroom mapRow( ResultSet rs,  int i) throws SQLException {
        log.trace("Build Row " + i );
        return Bedroom.builder()
            .id                          (rs.getLong  ("id"))
            .description                 (rs.getString("description"))
            .type(Bedroom.Type.toEnum    (rs.getString("type")))
            .gender(Bedroom.Gender.toEnum(rs.getString("gender"))).build();
    }

}