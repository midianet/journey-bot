package midianet.journey.repository;

import midianet.journey.domain.Payment;
import midianet.journey.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class PaymentRepository {
    private Logger log = LoggerFactory.getLogger(getClass());

    private StringBuilder sqlDefault = new StringBuilder()
        .append("select id,")
        .append("       date,")
        .append("       date_low,")
        .append("       amount,")
        .append("       person_id")
        .append("  from payment ");

    @Autowired
    private NamedParameterJdbcTemplate jdbc;
    
    public List<Payment> findByPerson(Long id){
	    log.trace("Request findByPerson " + id);
		String sql = sqlDefault.toString() + "where person_id = :id";
	    Map<String,Object> param = new HashMap();
	    param.put("id",id);
	    log.debug(sql + " " + id);
	    return jdbc.query(sql.toString(),param, this::mapRow);
    }

    private Payment mapRow( ResultSet rs,  int i) throws SQLException {
        log.trace("Build Row " + i );
        Payment p = Payment.builder()
            .id    (rs.getLong("id"))
			.date  (rs.getDate("date").toLocalDate())
			.amount(rs.getBigDecimal("amount"))
			.person(Person.builder().id(rs.getLong("person_id")) .build())
		    .build();
	    Optional.ofNullable(rs.getDate("date_low")).ifPresent(date -> p.setDateLow(date.toLocalDate()));
	    return p;
    }

}