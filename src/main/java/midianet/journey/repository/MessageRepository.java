package midianet.journey.repository;

import midianet.journey.domain.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class MessageRepository {
    private Logger log = LoggerFactory.getLogger(getClass());

    private StringBuilder sqlDefault = new StringBuilder()
        .append("select telegram,")
        .append("       message")
        .append("  from message");

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    public Optional<Message> findByTelegram(Long telegram){
        log.trace("Request findByTelegram Telegram " + telegram);
        StringBuilder sql = new StringBuilder(sqlDefault.toString())
           .append(" where telegram = :telegram");
        Map<String,Object> param = new HashMap();
        param.put("telegram",telegram);
        log.debug(sql.toString() + " " + telegram);
        try{
            return Optional.of(jdbc.queryForObject(sql.toString(),param, this::mapRow));
        }catch(EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void save(Message message) {
        log.trace("Request save Message");
        Map params = new HashMap();
        params.put("telegram", message.getTelegram());
        params.put("message" , message.getMessage());
        log.debug ("Params " + params.toString());
        if(findByTelegram(message.getTelegram()).isPresent()){
            jdbc.update("update message set message = :message where telegram = :telegram",params);
        }else{
            jdbc.update("insert into message(telegram,message)values(:telegram,:message)",params);
        }
    }

    private Message mapRow( ResultSet rs,  int i) throws SQLException {
        log.trace("Build Row " + i );
        return Message.builder().telegram(rs.getLong("telegram")).message(rs.getInt("message")).build();
    }

}