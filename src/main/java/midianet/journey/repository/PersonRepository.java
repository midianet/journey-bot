package midianet.journey.repository;

import midianet.journey.domain.Bedroom;
import midianet.journey.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class PersonRepository {
    private Logger log = LoggerFactory.getLogger(getClass());

    private StringBuilder sqlDefault = new StringBuilder()
        .append("select id,")
        .append("       name,")
        .append("       nickname,")
        .append("       phone,")
        .append("       telegram,")
        .append("       cpf,")
        .append("       birthday,")
        .append("       rg,")
        .append("       rg_exped,")
        .append("       register,")
        .append("       sex,")
        .append("       assent,")
        .append("       agreed,")
        .append("       state,")
        .append("       bedroom_id")
        .append("  from person");

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private SimpleJdbcInsert jdbcInsert;

    @Autowired
    public PersonRepository(JdbcTemplate template) {
        jdbcInsert = new SimpleJdbcInsert(template)
            .withTableName("person")
            .usingColumns ("name","nickname","telegram", "register","sex", "state","agreed")
            .usingGeneratedKeyColumns("id");
    }

    public List<Person> listAll() {
        log.trace("Request listAll Person");
        StringBuilder sql = new StringBuilder(sqlDefault.toString())
            .append(" order by register");
        log.debug("query:" + sql.toString());
        return jdbc.query(sql.toString(), this::mapRow);
    }

    public List<Person> listByBedroom(Long id) {
        log.trace("Request list by Bedroom " + id);
        StringBuilder sql = new StringBuilder(sqlDefault.toString())
           .append(" where bedroom_id = :id")
           .append(" order by name");
        Map<String,Object> param = new HashMap();
        param.put("id",id);
        log.debug(sql.toString() + "  " + id);
        return jdbc.query(sql.toString(),param, this::mapRow);
    }

    public Optional<Person> findById(Long id){
        log.trace("Request findById Person " + id);
        StringBuilder sql = new StringBuilder(sqlDefault.toString())
           .append(" where id = :id");
        Map<String,Object> param = new HashMap();
        param.put("id",id);
        log.debug(sql.toString() + " " + id);
        try{
            return Optional.of(jdbc.queryForObject(sql.toString(),param, this::mapRow));
        }catch(EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    public Optional<Person> findByTelegram(Long telegram){
        log.trace("Request findByTelegram" + telegram);
        StringBuilder sql = new StringBuilder(sqlDefault.toString())
           .append(" where telegram = :telegram");
        Map<String,Object> param = new HashMap();
        param.put("telegram",telegram);
        log.debug(sql.toString() + " " + telegram);
        try{
            return Optional.of(jdbc.queryForObject(sql.toString(),param, this::mapRow));
        }catch(EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }
    
    @Transactional
    public void updateSubscrible(Long telegram, Person.Contract state){
        log.trace("update Subscrible ");
        String sql = "update person set agreed = :state where telegram = :telegram";
        Map<String,Object> params = new HashMap();
        params.put("telegram",telegram);
        params.put("state",state.getValue());
        log.debug("Params " + params.toString());
        jdbc.update(sql,params);
    }

    @Transactional(rollbackFor = Exception.class)
    public Person insert(Person person) {
        log.trace("Request save Person");
        Map params = new HashMap();
        params.put("name"    , person.getName());
        params.put("telegram", person.getTelegram());
        params.put("nickname", person.getName());
        params.put("register", new Date());
	    params.put("sex"     , Person.Sex.MALE.getValue());
	    params.put("state"   , Person.State.WAITING.getValue());
	    params.put("agreed"  , Person.Contract.BLANK.getValue());
        log.debug ("Params " + params.toString());
        person.setId(jdbcInsert.executeAndReturnKey(params).longValue());
        return person;
    }

    private Person mapRow( ResultSet rs,  int i) throws SQLException {
        log.trace("Build Row " + i );
        Person p = Person.builder()
            .id                           (rs.getLong    ("id"))
            .name                         (rs.getString  ("name"))
            .nickname                     (rs.getString  ("nickname"))
            .phone                        (rs.getString  ("phone"))
            .telegram                     (rs.getLong    ("telegram"))
            .cpf                          (rs.getString  ("cpf"))
            .rg                           (rs.getString  ("rg"))
            .rgexped                      (rs.getString  ("rg_exped"))
            .sex(Person.Sex.toEnum        (rs.getString  ("sex")))
            .agreed(Person.Contract.toEnum(rs.getString  ("agreed")))
            .state (Person.State.toEnum   (rs.getString  ("state"))).build();
        Optional.ofNullable(rs.getObject("bedroom_id")).ifPresent(o    -> p.setBedroom(Bedroom.builder().id((Long)o).build()));
        Optional.ofNullable(rs.getDate  ("birthday"  )).ifPresent(date -> p.setBirthday(date.toLocalDate()));
        Optional.ofNullable(rs.getDate  ("register"  )).ifPresent(date -> p.setRegister(date.toLocalDate()));
        Optional.ofNullable(rs.getObject("assent"    )).ifPresent(o    -> p.setAssent((Integer)o));
        return p;
    }

}