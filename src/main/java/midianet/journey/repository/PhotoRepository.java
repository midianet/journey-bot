package midianet.journey.repository;

import midianet.journey.domain.Photo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.support.SqlLobValue;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

//import java.sql.ResultSet;
//import java.sql.SQLException;
import java.io.ByteArrayInputStream;
import java.sql.Types;
import java.util.*;

@Repository
public class PhotoRepository {
    private Logger log = LoggerFactory.getLogger(getClass());

    private StringBuilder sqlDefault = new StringBuilder()
        .append("select id,")
        .append("       photo,")
        .append("       date,")
        .append("       telegram")
        .append("  from photo");

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private SimpleJdbcInsert jdbcInsert;

//    public List<Photo> listAll() {
//        log.trace("Request listAll Photo");
//        StringBuilder sql = new StringBuilder(sqlDefault.toString())
//            .append(" order by date");
//        log.debug("query:" + sql.toString());
//        return jdbc.query(sql.toString(), this::mapRow);
//    }
//
//    public List<Photo> listByTelegram(Long telegram) {
//        log.trace("Request list by Telegram " + telegram);
//        StringBuilder sql = new StringBuilder(sqlDefault.toString())
//           .append(" where telegram = :telegram")
//           .append(" order by date desc");
//        Map<String,Object> param = new HashMap();
//        param.put("telegram",telegram);
//        log.debug(sql.toString() + "  " + telegram);
//        return jdbc.query(sql.toString(),param, this::mapRow);
//    }

//    public Optional<Photo> findById(Long id){
//        log.trace("Request findById Photo " + id);
//        StringBuilder sql = new StringBuilder(sqlDefault.toString())
//           .append(" where id = :id");
//        Map<String,Object> param = new HashMap();
//        param.put("id",id);
//        log.debug(sql.toString() + " " + id);
//        try{
//            return Optional.of(jdbc.queryForObject(sql.toString(),param, this::mapRow));
//        }catch(EmptyResultDataAccessException e){
//            return Optional.empty();
//        }
//    }

    @Transactional(rollbackFor = Exception.class)
    public Photo insert(Photo photo) {
        log.trace("Request save Photo");
        try{
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("photo", new SqlLobValue(new ByteArrayInputStream(photo.getPhoto()), photo.getPhoto().length, new DefaultLobHandler()), Types.BLOB);
            parameters.addValue("telegram", photo.getTelegram(),Types.NUMERIC);
            jdbc.update("insert into photo(telegram,date,photo)values(:telegram,now(),:photo)", parameters);
        } catch(Exception e) {
            e.printStackTrace();
        }
    
//        Map params = new HashMap();
//        params.put("telegram", photo.getTelegram());
//        params.put("photo"   , photo.getPhoto());
//        params.put("date"    , new Date());
//        log.debug ("Params " + params.toString());
//        photo.setId(jdbcInsert.executeAndReturnKey(params).longValue());
        return photo;
    }

//    private Photo mapRow( ResultSet rs,  int i) throws SQLException {
//        log.trace("Build Row " + i );
//        return Photo.builder()
//            .id                           (rs.getLong    ("id"))
//            .telegram                     (rs.getLong    ("telegram"))
//            .date                         (rs.getDate("date").toLocalDate())
//            .photo                        (rs.getBytes   ("photo")).build();
//    }

}