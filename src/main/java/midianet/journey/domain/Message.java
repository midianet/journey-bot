package midianet.journey.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Message {
    private Long    telegram;
    private Integer message;
}