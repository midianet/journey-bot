package midianet.journey.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.time.LocalDate;

@Data
@Builder
public class Person {
    private Long id;
    private Long telegram;
    private String name;
    private String nickname;
    private String phone;
    private String cpf;
    private String rg;
    private String rgexped;
    private LocalDate register;
    private Integer assent;
    private LocalDate birthday;
    private Sex sex;
    private Contract agreed;
    private State state;
    private Bedroom bedroom;
    
    @Getter
    @AllArgsConstructor
    public enum Sex {
        MALE  ("M","Masculino"),
        FEMALE("F","Feminino");
        private String value;
        private String description;
   
        public static Sex toEnum(String value){
            for (Sex e : Sex.values()) {
                if(e.value.equals(value)){
                    return e;
                }
            }
            return null;
        }
        
    }
    
    @Getter
    @AllArgsConstructor
    public enum State{
        WAITING   ("W","Candidato"),
        SELECTED  ("S","Selecionado"),
        ASSOCIATE ("A","Associado"),
        REGISTERED("R","Registrado"),
        CONFIRMED ("C","Confirmado");
        private String value;
        private String description;
    
        public static State toEnum(String value) {
            for (State e : State.values()) {
                if(e.value.equals(value)){
                    return e;
                }
            }
            return null;
        }
        
    }
    
    @Getter
    @AllArgsConstructor
    public enum Contract{
        BLANK  ("N" ,"Não Assinado"),
        AGREE  ("A","Aceito"),
        DISAGRE("D","Não Aceito");
        private String value;
        private String description;
    
        public static Contract toEnum(String value) {
            for (Contract e : Contract.values()) {
                if(e.value.equals(value)){
                    return e;
                }
            }
            return null;
        }
        
    }
	
}