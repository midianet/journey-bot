package midianet.journey.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.List;

@Data
@Builder
public class Bedroom {
    private Long id;
    private String description;
    private List<Person> occupants;
    private Type         type;
    private Gender       gender;
    
    @Getter
    @AllArgsConstructor
    public enum Type{
        DOUBLE    ("D","Quarto Duplo"),
        TRIPLE    ("T","Quarto Triplo"),
        QUADRUPLE ("Q","Quarto Quádruplo");
        private String value;
        private String description;
        
        public static Bedroom.Type toEnum(String value){
            for (Bedroom.Type e : Bedroom.Type.values()) {
                if(e.value.equals(value)){
                    return e;
                }
            }
            return null;
        }
        
    }
    
    @Getter
    @AllArgsConstructor
    public enum Gender{
        ROOM_FEMALE ("F","Feminino"),
        ROOM_MALE   ("M","Masculino"),
        ROOM_COUPLE ("U","Misto");
    
        private String value;
        private String description;
    
        public static Bedroom.Gender toEnum(String value){
            for (Bedroom.Gender e : Bedroom.Gender.values()) {
                if(e.value.equals(value)){
                    return e;
                }
            }
            return null;
        }
        
    }
    
}