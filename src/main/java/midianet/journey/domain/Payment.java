package midianet.journey.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
public class Payment {
    private Long id;
    private LocalDate date;
    private LocalDate dateLow;
    private BigDecimal amount;
    private Person person;
}