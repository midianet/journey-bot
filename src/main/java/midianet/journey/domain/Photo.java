package midianet.journey.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Photo {
    private Long id;
    private byte[] photo;
    private LocalDate date;
    private Long    telegram;
}